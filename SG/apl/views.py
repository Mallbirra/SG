# Create your views here.
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from apl.forms import *
from apl.models import *

def raza_list(request):
    data = {
        'title': 'Listado de Raza',
        'raza': Raza.objects.all()
    }
    return render(request, 'Raza/list.html', data)


def post(request, *args, **kwargs):
    data = {}
    try:
        data = Raza.objects.get(pk=request.POST['id']).toJSON()
    except Exception as e:
        data['error'] = str(e)
    return JsonResponse(data)

class RazaListView(ListView):
    model = Raza
    template_name = 'Raza/list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['create_url'] = reverse_lazy('apl:raza_agregar')
        context['title'] = 'Listado de Raza'
        return context


class RazaCreateView(CreateView):
    model = Raza
    form_class = RazaForm
    template_name = 'Raza/create.html'
    success_url = reverse_lazy('apl:raza_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Agregar una Raza'
        return context


class RazaUpdateView(UpdateView):
    model = Raza
    form_class = RazaForm
    template_name = 'Raza/create.html'
    success_url = reverse_lazy('apl:raza_list')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición una Raza'
        context['entity'] = 'raza'
        context['list_url'] = reverse_lazy('apl:raza_list')
        context['action'] = 'edit'
        return context


class RazaDeleteView(DeleteView):
    model = Raza
    template_name = 'Raza/delete.html'
    success_url = reverse_lazy('apl:raza_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Raza'
        context['entity'] = 'razas'
        context['list_url'] = reverse_lazy('apl:raza_list')
        return context