from django.urls import path
from apl.views import *

app_name = 'apl'

urlpatterns = [
    path('raza/list/', RazaListView.as_view(), name='raza_list'),
    path('raza/agregar/', RazaCreateView.as_view(), name='raza_agregar'),
    path('raza/modificar/<int:pk>/', RazaUpdateView.as_view(), name='raza_modificar'),
    path('raza/eliminar/<int:pk>/', RazaDeleteView.as_view(), name='raza_eliminar'),
    # path('vientre/list/', VientreListView.as_view(), name='vientre_list'),
    # path('palpacion/list/', PalpacionListView.as_view(), name='palpacion_list'),
    # path('raza/list/', AnimalListView.as_view(), name='animal_list'),
    # path('perdida/list/', PerdidaListView.as_view(), name='perdida_list'),
    # path('nacimiento/list/', NacimientoListView.as_view(), name='nacimiento_list'),
    # path('fallecimiento/list/', FallecimientoListView.as_view(), name='fallecimiento_list'),
    # path('marcado/list/', MarcadoListView.as_view(), name='marcado_list'),
    # path('medida/list/', MedidaListView.as_view(), name='medida_list'),
    # path('tipo_tprev/list/', TipoTPrevListView.as_view(), name='tipo_tprev_list'),
    # path('tpreventivo/list/', TPreventivoListView.as_view(), name='tpreventivo_list'),
    # path('tcurativo/list/', TCurativoListView.as_view(), name='tcurativo_list'),
    # path('aplicacion_tprev/list/', AplicacionTPrevListView.as_view(), name='aplicacion_tprev_list'),
    # path('comprador/list/', CompradorListView.as_view(), name='comprador_list'),
    # path('lote_venta/list/', LoteVentaListView.as_view(), name='lote_venta_list'),
    # path('animales_lote/list/', AnimalesLoteListView.as_view(), name='animales_lote_list'),
    # path('articulo/list/', ArticuloListView.as_view(), name='articulo_list'),
    # path('compra/list/', CompraListView.as_view(), name='compra_list'),
    # path('detalle_compra/list/', DetalleCompraListView.as_view(), name='detalle_compra_list'),

]
