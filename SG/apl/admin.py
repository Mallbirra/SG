from django.contrib import admin
from apl.models import *

# Register your models here.
admin.site.register(Raza)
admin.site.register(Animal)
admin.site.register(Palpacion)
admin.site.register(Vientre)